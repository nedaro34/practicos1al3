let buttonAgregar = document.getElementById('agregarProfesor');
let buttonEliminar = document.getElementById('eliminarProfesor');
let selectProfesorExpositorCurso = document.getElementById('profesorExpositorCurso');
let selectProfesoresExpositoresCurso = document.getElementById('profesoresExpositoresCurso');
let buttonGuardar = document.getElementById('guardar');
buttonAgregar.addEventListener('click', agregarProfesor);
buttonEliminar.addEventListener('click', eliminarProfesor);
buttonGuardar.addEventListener('submit', subirSeleccionados);

function agregarProfesor(){
    let listado = Array.from(selectProfesorExpositorCurso.selectedOptions);
    for(elementoSeleccionado of listado){
        selectProfesorExpositorCurso.remove(elementoSeleccionado.index);
        selectProfesoresExpositoresCurso.add(elementoSeleccionado);
    }
}

function eliminarProfesor(){
    let listado = Array.from(selectProfesoresExpositoresCurso.selectedOptions);
    for(elementoSeleccionado of listado){
        selectProfesoresExpositoresCurso.remove(elementoSeleccionado.index);
        selectProfesorExpositorCurso.add(elementoSeleccionado)
    }
}

function subirSeleccionados(){
    for (elementoSeleccionado of selectProfesoresExpositoresCurso.options){
        elementoSeleccionado.selected = "true";
    }
}