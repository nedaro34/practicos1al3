let textCantidadMinimaAlumnos = document.getElementById('cantidadMinimaAlumnos');
let textCantidadMaximaAlumnos = document.getElementById('cantidadMaximaAlumnos');
textCantidadMinimaAlumnos.addEventListener('change', validacionCantidadAlumnos);
textCantidadMaximaAlumnos.addEventListener('change', validacionCantidadAlumnos);

function validacionCantidadAlumnos(event){
    if(textCantidadMinimaAlumnos.value!="" && textCantidadMaximaAlumnos.value!=""){
        if(Number(textCantidadMinimaAlumnos.value)>Number(textCantidadMaximaAlumnos.value)){
            alert(`La cantidad mínima de alumnos (${textCantidadMinimaAlumnos.value}) no puede ser mayor que la cantidad máxima (${textCantidadMaximaAlumnos.value})`);
            event.preventDefault();
        }
    }
}

let textFechaInicioCurso = document.getElementById('fechaInicioCurso');
let textFechaFinCurso = document.getElementById('fechaFinCurso');
fechaInicioCurso.addEventListener('change', validacionFechas);
fechaFinCurso.addEventListener('change', validacionFechas);

function validacionFechas(event){
    let fechaInicioCurso = new Date(textFechaInicioCurso.value);
    let fechaFinCurso = new Date(textFechaFinCurso.value);
    if(fechaInicioCurso!='Invalid Date' && fechaFinCurso!='Invalid Date'){
        if(fechaInicioCurso>fechaFinCurso){
            alert(`La fecha de inicio del curso (${fechaInicioCurso.toLocaleDateString()}) no puede ser posterior a la fecha de finalización del curso (${fechaFinCurso.toLocaleDateString()})`)
            event.preventDefault();
        }
    }
}

let formulario = document.getElementById('formularioCurso');
formulario.addEventListener('submit', validacionFormularioCurso);

function validacionFormularioCurso(event){
    validacionFechas(event);
    validacionCantidadAlumnos(event);
    //agregar más métodos para validar el resto de campos antes de subir los datos al servidor
}
